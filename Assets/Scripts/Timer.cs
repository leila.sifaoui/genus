﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public Text timerText;
    public Text gameOverText;
    public Text scoreText;
    private float timer = 15f;

    public float startTime;
    private bool gameover = false;


    // Start is called before the first frame update
    void Start()
    {
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (gameover)
        {
            gameOverText.text = "Game Over";
            return;
        }
        else if (timer <= 0)
        {
            gameOverText.text = "You Win";
        }
        else
        {
            float t = Time.time - startTime;
            string minutes = ((int)t / 60).ToString();
            string seconds = (t % 60).ToString("f1");
            timerText.text = minutes + ":" + seconds;
        }
    }

    public void GameOver()
    {
        gameover = true;
        scoreText.text = "Score : ";
        gameOverText.text = "Game Over";
        timerText.color = Color.yellow;
    }
}
