﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testShield : MonoBehaviour
{
    public AchievementManager achievementManager;

    Vector3 target;
    //float speed = 5.0f;

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Destroy(GameObject.FindGameObjectWithTag("Player"));
            achievementManager.UnlockAchievementYouDied(achievements.YouDied);
        }
        
    }
}