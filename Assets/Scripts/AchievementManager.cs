﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementManager : MonoBehaviour
{
    public AchievementDatabase database;

    public AchievementNotificationController achievementNotificationController;

    public GameObject achievementItemPrefab;
    public Transform content;



    //YOU DIED Achievement --------------------------------------------------------------
    private achievements achievementYouDied = achievements.YouDied;
    //YOU DIED Achievement --------------------------------------------------------------



    //private achievements achievementToShow;

    [SerializeField][HideInInspector]
    private List<AchievementItemController> achievementItems;



    public void Start()
    {
        LoadAchievementsTable();
    }

   

    [ContextMenu("LoadAchievementsTable()")]
    private void LoadAchievementsTable()
    {
        foreach (AchievementItemController controller in achievementItems)
        {
            DestroyImmediate(controller.gameObject);
        }
        achievementItems.Clear();
        foreach (Achievement achievement in database.achievements)
        {
            GameObject obj = Instantiate(achievementItemPrefab, content);
            AchievementItemController controller = obj.GetComponent<AchievementItemController>();
            bool unlocked = PlayerPrefs.GetInt(achievement.id, 0) == 1;

            controller.unlocked = unlocked;
            controller.achievement = achievement;

            controller.RefreshView();
            achievementItems.Add(controller);
        }
    }

   



    //YOU DIED Achievement --------------------------------------------------------------




    public void ShowNotificationYouDied()
    {
        Achievement achievement = database.achievements[(int)achievementYouDied];
        achievementNotificationController.ShowNotification(achievement);
    }
    public void UnlockAchievementYouDied()
    {
        UnlockAchievementYouDied(achievementYouDied);
    }





    public void UnlockAchievementYouDied(achievements achievement)
    {
        AchievementItemController item = achievementItems[(int)achievement];
        if (item.unlocked)
            return;

        ShowNotificationYouDied();
        PlayerPrefs.SetInt(item.achievement.id, 1);
        item.unlocked = true;
        item.RefreshView();
    }




    //YOU DIED Achievement ---------------------------------------------------------------


    public void LockAllAchievements()
    {
        foreach (Achievement achievement in database.achievements)
        {
            PlayerPrefs.DeleteKey(achievement.id);
        }
        foreach (AchievementItemController controller in achievementItems)
        {
            controller.unlocked = false;
            controller.RefreshView();
        }
    }








    /*public void ShowNotification()
    {
        Achievement achievement = database.achievements[(int)achievementToShow];
        achievementNotificationController.ShowNotification(achievement);
    }
    public void UnlockAchievement()
    {
        UnlockAchievement(achievementToShow);
    }

    public void UnlockAchievement(achievements achievement)
    {
        AchievementItemController item = achievementItems[(int)achievement];
        if (item.unlocked)
            return;

        ShowNotification();
        PlayerPrefs.SetInt(item.achievement.id, 1);
        item.unlocked = true;
        item.RefreshView();
    }*/


}
