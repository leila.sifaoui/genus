﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaRising : MonoBehaviour
{

    Vector3 target;
    float speed = 5.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0, speed * Time.deltaTime, 0);
    }


    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Destroy(GameObject.FindGameObjectWithTag("Player"));
        }
        else if (other.gameObject.CompareTag("FloorsLava"))
        {
            Destroy(GameObject.FindGameObjectWithTag("FloorsLava"));
        }
    }
}
