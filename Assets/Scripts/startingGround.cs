﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class startingGround : MonoBehaviour
{
    public Rigidbody rb;


    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            StartCoroutine("fallDown");
        }
    }

    IEnumerator fallDown()
    {
        yield return new WaitForSeconds(5f);
        rb.isKinematic = false;
    }
}
