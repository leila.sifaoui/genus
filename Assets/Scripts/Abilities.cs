﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Abilities : MonoBehaviour
{
    //public Texture2D abilityIcon;
    public GameObject shield;
    public GameObject shieldIcon;
    public float duration;
    float abilityTimer;

    public float cooldownTime;
    private float nextFireTime = 0;


    void Start()
    {
        abilityTimer = duration;
    }


    void Update()
    {

        bool AB1 = Input.GetKeyDown(KeyCode.E);

        if (Time.time > nextFireTime)
        {
            if (AB1)
            {
                Ability1();
                shieldIcon.gameObject.SetActive(false);
                nextFireTime = Time.time + cooldownTime;
                StartCoroutine("shieldBack");

            }
        }

    }

    /*private void OnGUI()
    {
        GUI.DrawTexture(new Rect(10, 150, abilityIcon.width, abilityIcon.height), abilityIcon);
    }*/

    void Ability1()
    {
        GameObject myShield = (GameObject) Instantiate (shield, transform.position, Quaternion.identity);
        Shield shieldScript = myShield.GetComponent<Shield>();
        shieldScript.myOwner = this.gameObject;
    }

    IEnumerator shieldBack()
    {
        yield return new WaitForSeconds(10f);
        shieldIcon.gameObject.SetActive(true);
    }

}
